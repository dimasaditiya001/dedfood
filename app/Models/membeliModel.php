<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class membeliModel extends Model
{
    use HasFactory;
    protected $table = 'membeli';
    protected $fillable=[
        'barang_id',
        'akun_id',
        'jumlah',
    ];

}
