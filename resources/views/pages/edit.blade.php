@extends('template.nav')
@section('container')
<div class="container">
    <h1 class="my-3">Edit Data {{explode(' ', trim($title ))[0]}}</h1>
    <form action="/{{explode(' ', strtolower(trim($title)))[0]}}/update/{{$content['id']}}" method="post">
      @csrf
      @php $i = 0 @endphp
      @foreach ($col as $c)
      @php    
      $co = $cols["$i"];
      @endphp
        <label for="{{explode(' ', trim($c ))[0]}}" class="form-label">{{$c}}</label>
        <input type="text" name="{{explode(' ', trim($c ))[0]}}" class="form-control" value="{{$content["$co"]}}">
        @if ($foreign != null)
            {{$foreign}}
        @endif
        @php
            $i++;
        @endphp
        @endforeach
        <input type="submit" value="Update Data" class="btn btn-primary mt-3">
    </form>
</div>
@endsection

@foreach ($for as $f)
    
@endforeach